/*
	IRC-Bot mit Voting-Schnittstelle, Benachrichtigungs-System und Benutzer-Prüfung

	Der Bot verbindet sich über eine REST-API mit einer Datenbank,
	um die Rechte der Nutzer im IRC zu überprüfen und dadurch
	Abstimmungen in einem IRC-Chat zu realisieren.

	Die Überprüfung der Benutzerrechte geschieht in zwei Schritten:
	Prüfung 1 sucht nach dem Namen des IRC-Users in einer MySQL-Tabelle, 
	welche ebenfalls von der Forum-Software "xenForo" benutzt wird
	und speichert den Namen des Nutzers bei Vorhandensein eines Accounts.

	Prüfung 2 sucht dann (sollte Prüfung 1 keinen Treffer hervorgebracht
	haben) in einer anderen Tabelle nach einer "Übersetzung" von IRC- in
	xenForo-Usernamen, welche ebenfalls die Privilegien für den IRC-Chat
	beinhaltet.
	Sollten IRC- und xenForo-Accountname eines Nutzers abweichen, kann er
	dies in einer Benutzerverwaltung eintragen und somit seine Rechte auf
	seinen andersnamigen IRC-Account "übertragen". War Prüfung 1
	erfolgreich, wird die Übersetzung übersprungen und lediglich der Rang
	des Nutzers überprüft.
	
	Dieser Bot wird aktuell (in einer CoffeeScript-Version auf Heroku)
	für den Twitch.TV-Chat eines Livestreams verwendet,
	welcher lediglich einen IRC-"Aufsatz" bietet (anstelle eines
	"regulären" Standard-IRC-Server) der, auf Grund
	einiger abweichenden Verhaltensweisen und Server-Antworten,
	eines Workarounds bedarf.

	(Kommentare die diese Problematik erleutern, beginnen mit "/**".)
*/

// Importierte Bibliotheken:
// IRC-Schnittstelle -		Ermöglicht der Anwendung sich mit einem IRC-Server + -Raum zu verbinden
var irc				= require("irc");			// Docs: https://node-irc.readthedocs.org/en/latest/API.html

// HTTP-Schnittstelle -		Sorgt für HTTP-GET und -POST-Anforderungen
var http			= require('http');			// Docs: http://nodejs.org/api/http.html

/* "querystring" ist für die Umwandlung von assoziativen Arrays
	in HTTP-Querystrings verantwortlich (.stringify();)*/
var querystring		= require('querystring');	// Docs: http://nodejs.org/api/querystring.html

/* "util" erweitert node.js um gewisse "allgemeine" Funktionen, welche in dieser Anwendung beispielsw.
	das Strukturieren von Strings vereinfacht, (util.format();) und den Code einfacher lesbar macht.
	(Entspricht in etwa der cpp-Funktion "sprintf")
	Beispiel:
		Vorher:
		'At '+timeFinal+' user "'+chunk[1][i].username+'" submitted "'+chunk[1][i].title+'": '+chunk[1][i].url);
		Jetzt:
		util.format('At %s user "%s" submitted "%s": %s', timeFinal, chunk[1][i].username, chunk[1][i].title, chunk[1][i].url);
	*/
var util			= require('util');			// Docs: http://nodejs.org/api/util.html

function Vote () {
	/*
		Sorgt für globale Erreichbarkeit von Funktionen und Variablen dieser Klasse

		Vote-Objekt:
			- Funktion_A
			- Funktion_B

		Funktion_A kann dann in Funktion_B über that.Funktion_A erreicht werden.
	*/
	that = this;

	this.active				= false;

	// Enthält einen multidimensionalen Array:
	// this.users[NAME_EINES_USERS] = ENTSCHEIDUNG_IN_DER_ABSTIMMUNG
	this.users				= Array();

	// Konfiguration für die IRC-Schnittstelle
	this.config				= {
		channels:				["#deliciouscinnamon"],
		server:					"deliciouscinnamon.jtvirc.com",
		nick:					"dcbot",
		options:				{
			// --- Mögliche Authentifizierung ---
			/*password:	"",*/
			userName: 'dcbot',
			// ----------------------------------
		    port: 6667,
		    //debug: true,

		    // floodProtection verzögert Nachrichten um
		    // einen Rauswurf wegen Spam zu verhindern.
    		floodProtection: true,
    		floodProtectionDelay: 2000,
		}
	};

	// Login-Daten für die HTTP-Anforderungen, welche bei einem Request in den Base64-kodierten HTTP-"basic authentication header" konvertiert werden
	this.httplogin			= {
		user: "DC_Bot",
		pass: "po6Dp5DgavNYPjKQiMhs99UjIWvz2bgr3YKpG4LZJncLRtqLS7D3UMM8OBFp8JJeSnxHkEpDEHgHLE1e"
	}

	// Verantwortlich für alle Requests, die Abstimmungen betreffen.
	this.serverRequest		= function (usedMethod, args, callback) {

		/*
			Setzt die Nutzerrechte auf den Integer "0", wenn keine Alternative vorhanden ist.
			(JavaScript macht sonst NULL draus und zerschießt damit den HTTP-Request.)
		*/
		if (usedMethod == "privileges")
			args.userprivileges	= undefined;

		// Strukturiert die HTTP-query.
		data			= querystring.stringify({
			method:			usedMethod,
			arguments:		JSON.stringify(args)
		});

		console.log("-------- HTTP-REQUEST: START\n"+data); // (Debug)

		// Finaler "basic authentication header".
		that.httplogin.final = 'Basic ' + new Buffer(that.httplogin.user + ':' + that.httplogin.pass).toString('base64');

		// Optionen für den HTTP-request
		var options		= {
		host: 'www.deliciouscinnamon.com',
		port: 80,
		path: '/vote/API.php',
		method: 'POST',
		headers: {
				'Content-Type': 'application/x-www-form-urlencoded',
				'Content-Length': data.length,
				'Authorization': that.httplogin.final
			}
		};

		// HTTP-Request:
		var req = http.request(options, function(res) {
			res.setEncoding('utf8');
			// Callback welches mit der Antwort des Servers aufgerufen wird
			res.on('data', function (chunk) {
				console.log("-------- HTTP-REQUEST: DONE\n"+unescape(chunk)); // (Debug)

				// Die API antwortet in jedem Fall mit JSON, also parsen.
				// Für das Format der Antwort siehe 
				chunk = JSON.parse(chunk);

				// Wenn alles glatt lief...
				if (chunk[0] != "error")
					// ...nach Methode filtern:
					switch (usedMethod) {
						case "privileges_s": // = "IRC-Name ungleich xenForo-Name"-Prüfung
							// Inhalt leer? (In JavaScript mit '== false' prüfbar - um nur auf 'false' zu prüfen wäre '=== false' von Nöten)
							if (chunk[1] == false)
								// Ja: Zweite HTTP-Anforderung (mit selben Parametern) starten - jedoch mit 'method' zu "privileges" ändern
								that.serverRequest("privileges", args, callback);
							else
								// Nein: Angegebenes Callback mit den Antworten des Servers als Parameter aufrufen.
								callback({
									username: chunk[1].irc_name,
									privileges: chunk[1].privileges
								});
							break;
						case "privileges": // = "Normale"-Prüfung
							// Inhalt leer? (In JavaScript mit '== false' prüfbar - um nur auf 'false' zu prüfen wäre '=== false' von Nöten)
							if (chunk[1] == false)
								// Ja: Callback ohne Namen und "-1" als Rang aufrufen.
								callback({
									username: "",
									privileges: -1
								});
							else
								// Nein: Angegebenes Callback mit den Antworten des Servers als Parameter aufrufen.
								callback({
									username: chunk[1].username,
									privileges: privileges
								});
							break;
						case "submit": // = Vote nach Erfolg in der DB speichern
							callback(); // Leeres Callback
							break;
					}
			});
		});

		// Inhalte der HTTP-Anforderung senden...
		req.write(data);
		// ...und schließen.
		req.end();
	}

	// Abstimmung eines Nutzer speichern
	// Geschieht lokal - nur das Endergebnis wird in der Datenbank gespeichert.
	this.submitVote			= function (user, votedFor) {
		// Abstimmung speichern, wenn Antwort gültig und Nutzer noch nicht abgestimmt hat.
		if (votedFor <= that.answers.length && !(user in that.users)) {
			that.users[user]		= true;
			that.answers[votedFor - 1] = that.answers[votedFor - 1] == undefined ? 1 : that.answers[votedFor - 1] + 1;
		}
	}

	// Verarbeitet die Chatzeile chat_row als Befehl
	// Syntax: @vote BEFEHL DAUER_IN_SEKUNDEN FRAGE,ANTWORT 1,,ANTWORT 2, [...]
	// "@vote " wird beim Empfangen der Zeile (siehe 'this.bot.addListener("message")') entfernt.
	this.parseChatRow		= function (chat_row, user) {
		// Zeile nach Leerzeichen trennen
		chat_row		= chat_row.split(" ");
		// Ersten Eintrag als "Befehl" speichern...
		command			= chat_row.shift();
		// ...und auswerten.
		switch (command) {
			case "create":		// = Abstimmung starten
				// Verhindert starten von mehreren Votes.
				if (!that.active) {
					that.active = true;

					// Nächsten Eintrag der ge'.split()'eten Chatzeile als Abstimmungsdauer speichern.
					that.duration			= parseInt(chat_row.shift());
					/*
						Anmerkung zur Trennung: Die doppelte Trennung (zunächst mit Leerzeichen, nun mit Kommata) kam auf Wunsch des Nutzers.
							Eine einfache Trennung nach "," wäre möglich und einfacher.
							Beispiel: (@vote create,30,FRAGE MIT LEERZEICHEN,ANTWORT 1 MIT LEERZEICHEN,ANTWORT 2[...])
					*/
					// Chatzeile wieder zusammensetzen und nun nach "," trennen. (Ermöglicht Antworten und Fragen mit Leerzeichen.)
					chat_row				= chat_row.join(" ").split(",");

					// Ersten Eintrag als "Frage" speichern...
					that.question			= chat_row.shift();
					// ...restlichen Array als Antwortmöglichkeiten speichern.
					that.dict				= chat_row;
					// Neuen leeren Array erstellen, welche die Anzahl an Nutzern für ein Ergebnis beinhaltet,
					that.answers			= new Array(that.dict.length);
					// Abstimmungsstarter vom Abstimmen ausschließen und neuen Array für "Nutzer die bereits abgestimmt haben" erstellen.
					that.users				= new Array(user);

					/*
						Anmerkung zur Vorgehensweise der Speicherung:
							Ursprünglich war angedacht, Nutzer und ihre Abstimmungen
							in einem assoziativen Array zu speichern (
								that.answers[ANTWORTMÖGLICHKEIT] = ["NUTZER A", "NUTZER C", "NUTZER J"]
							), doch assoziative Arrays können (im Vergleich zur einfach-nummerierten) in JS
							nicht mit .length gezählt werden. Die einzige Möglichkeit dies zu erreichen,
							wäre durch 'for(NUTZER in that.answers[ANTWORTMÖGLICHKEIT]) i++;', wobei
							'i' am Ende die gesammte Anzahl der Nutzer widerspiegelt, was bei einer
							größeren Nutzerzahl eine Verzögerung hervorruft.
					*/

					console.log(chat_row);

					// Vote starten wenn Dauer, Frage und Antwortmöglichkeiten vorhanden sind
					if (that.duration > 0 && that.question.length > 0 && that.dict.length > 0) {
						// Die Benutzerrechte des Users überprüfen, der den Vote starten will
						that.serverRequest("privileges_s", {username: user}, function(userInfo) { // Diese Funktion wird ausgeführt, wenn der Rechte-Check vorbei ist

							// Besitzt der Nutzer genügend Rechte...
							if (userInfo.privileges >= 100) {
								// ...leeren String erstellen...
								var finalOpt = "";
								// ...in diesen die Informationen über die Abstimmungsoptionen für die Chatnutzer einfügen...
								for (i = 0; i < that.dict.length; i++) {
									finalOpt += util.format("'@vote for %s' = %s\n", i+1, that.dict[i]);
								}
								// ...in der Konsole ausgeben... (Debug)
								console.log( util.format("'%s' started a Vote: (%s seconds)\n%s (Vote with:)\n %s", user, that.duration, that.question, finalOpt) );
								// ...in den Chat schreiben...
								that.bot.say(that.config.channels[0], util.format("'%s' started a Vote: (%s seconds)\n%s\n %s", user, that.duration, that.question, finalOpt) )
								// ...warten bis die Zeit vorbei ist, und dann den Vote beenden.
								setTimeout((function() {
	  								that.finishVote();
								}), that.duration * 1000);
							}
							// Bot für neue Votes freigeben, sollte der Nutzer nicht die ausreichenden Rechte besitzen
							else
								that.active = false;
						});
					}
					// Bot freigeben und Informationen leeren, sollten die angegebenen Infos ungültig sein
					else {
						that.active = false;
						that.duration			= undefined;
						chat_row				= undefined;
						that.question			= undefined;
						that.dict				= undefined;
						that.answers			= undefined;
						that.users				= undefined;
					}
						
				}
				break;
			case "for":		// = Abstimmen
				// Wenn ein Vote läuft...
				if (that.active)								
					// ...den letzten Rest der Chatzeile ("@vote for >>2<<") + Informationen über den Nutzer verarbeiten
					that.submitVote(user, parseInt(chat_row));
				break;
			case "stop":	// = Abstimmung stoppen
				// Rechte des Nutzers überprüfen...
				that.serverRequest("privileges_s", {username: user}, function(userInfo) {
					//...und Vote stoppen, wenn ausreichend Rechte
					if (userInfo.privileges >= 100)
						that.finishVote();
					else
						that.active = false;
				});
				break;
		}
	}

	// Beendet Votes
	this.finishVote			= function () {
		// Wenn ein Vote läuft...
		if (that.active) {
			// Ergebnisse via HTTP-Anforderung in der Datenbank speichern.
			that.serverRequest("submit", {
				duration:		that.duration,
				question:		that.question,
				answers:		that.dict,
				results:		that.answers
			}, function () {
				// Bot freigeben, wenn dies erledigt ist
				that.active = false;
			});
			// Ende des Votes...
			that.bot.say(that.config.channels[0], "Vote ended!");
			// ...und Ergebnisse verkünden.
			for (i = 0; i < that.answers.length; i++) {
				that.bot.say(that.config.channels[0], util.format("%s Votes for '%s'", (("000" + parseInt(that.answers[i] == undefined ? 0 : that.answers[i]))).slice(-3), that.dict[i]));
				/*
					Die Zeile
					("000" + parseInt(that.answers[i] == undefined ? 0 : that.answers[i]))).slice(-3)
					sorgt für Ordnung bei den Ergebnissen. Da die Ergebnisse nach Eintragung sortiert
					werden (ANTWORT 1 wird immer die erste Zeile sein, ANTWORT 2 die zweite, etc.),
					sorgen führende Nullen für Ordnung (Beispiel mit 8 Votes für ein Ergebnis)
						that.answers[i] == undefined ? 0 : that.answers[i]			= 8
						parseInt(	/\	)											= 8 (sicherstellen, dass wir mit Integers arbeiten)
						("000" +	/\ )											= "0008" (da alles Folgende zum type des ersten Summanden getypecasted wird)
									/\.slide(-3);									= "008" 
					und sorgt für leichter les- und vergleichbare Zeilen:
						"008 Votes for 'Answer 1'"
						"023 Votes for 'Answer 2'"
						"012 Votes for 'Answer 3'"
				*/
			}
		}	
	}

	// Überprüft auf neue Benachrichtigungen (aktuell Fanart und Spenden)
	this.notificationCheck	= function (task) {
		/*
			Da nicht alle Anwendungen die Möglichkeit haben Callbacks zu empfangen -
			und Callbacks einen Dienst vorraussetzen, der diese 24/7 an sämtliche
			Clients weiterleitet - agiert das Script von selbst, anstatt auf
			Benachrichtigungen zu reagieren.

			Backend:
			Wurde beispielsweise eine Spende getätigt, sendet PayPals IPN-Service
			(Instant Payment Notification) einen POST-Reqeust an eine .php-Datei.
			Diese überprüft den Aufruf auf Echtheit und speichert ihn - und alle
			dazugehörigen Daten (Beitrag der Spende, E-Mail Adressen, etc.) in
			der MySQL Datenbank. (Siehe )
			
			Komplett unabhängig von PayPals IPN oder dem Speicherungs-Script, 
			greift dieser Bot auf eine API zu, welche die letzten Reihen der
			Datenbank ausgibt, die seit dem letzten Aufruf neu dazu gekommen sind.
		*/

		console.log("-------- NOTIFICATIONCHECK START\n");	// (Debug)

		// Einstellungen für die unterschiedlichen HTTP-Anforderungen
		var options		= {
			fanart: {
				host: 'www.deliciouscinnamon.com',
				port: 80,
				path: '/fanart/bot_api.php',
				method: 'GET'
			},
			donations: {
				host: 'www.deliciouscinnamon.com',
				port: 80,
				path: '/ipn_receiver/log/',
				method: 'GET'
			}
		};

		// Benachrichtigugnen im util.format()-tauglichem Format
		var messages = {
			donations: [
				"NEW DONATIONS!",
				"%s %s from %s at %s",
				"Want to donate as well? Help us grow and get a shout out! Visit http://deliciouscinnamon.com/donate/ for more details!"
			],
			fanart: [
				"New fan-art!",
				"At %s user \"%s\" submitted \"%s\": %s",
				"Got some art? Upload it at http://www.deliciouscinnamon.com/fan-art/ and we'll take care of the rest!"
			]
		};

		// HTTP-Anforderung
		var req = http.request(options[task], function(res) {
			res.setEncoding('utf8');
			// Callback welches mit der Antwort des Servers aufgerufen wird
			res.on('data', function (chunk) {
				console.log("-------- NOTIFICATIONCHECK DONE\n"); // (Debug)
				// Die API antwortet in jedem Fall mit JSON, also parsen.
				// Für das Format der Antwort siehe 
				chunk = JSON.parse(chunk);
				console.log(chunk); // (Debug)
				// Kein Fehler und kein leerer Inhalt
				if (chunk[0] != "error" && chunk[1]) {
					// \|/ Trennung, da JS sonst mit "'undefined' is not an object" die Ausführung des Scripts beendet.
					if (chunk[1].length > 0) {
						// Enthält fertige Strings | Extra Variable dafür, damit Debug-Zeilen nur eine Variable ausgeben müssen
						var finalReturn = "";

						// Ankündigungsnachricht ausgeben
						that.bot.say(that.config.channels[0], messages[task][0]);

						for (i = 0; i < chunk[1].length; i++) {
							// Da hauptsächlich für US-Streamer entwickelt, US-Zeitformat erstellen...
							time = new Date(chunk[1][i].timestamp);
							// ...und in einer Variable speichern
							timeFinal =		util.format("%s:%s %s",
								time.getHours() % 12 ? time.getHours() : 12,
								time.getMinutes() > 9 ? time.getMinutes() : "0"+time.getMinutes(),
								time.getHours() >= 12 ? "PM" : "AM"
							);
							// Zweite Nachricht formatieren, entsprechende Variablen mit angeben und für jede neue Reihe in der DB ausgeben
							switch (task) {
								case "donations":
									finalReturn = util.format(messages[task][1], chunk[1][i].p_total, chunk[1][i].p_currency, chunk[1][i].d_username == "" ? "someone without a username" : '"'+chunk[1][i].d_username+'"', timeFinal);
									break;
								case "fanart":
									finalReturn = util.format(messages[task][1], [timeFinal, chunk[1][i].username, chunk[1][i].title, chunk[1][i].url]);
									break;
							}
							/*
								Optimalerweise wäre dieser switch gar nicht hier, da es aber keine
								Möglichkeit gibt 'util.format' bspw. einen Array mit Werten zu geben,
								die eingesetzt werden sollen, muss das Ganze mehr oder weniger
								hardgecoded werden.
								Siehe Docs für 'util(.format)'. (Verweis in Zeile 53)
							*/
							console.log(finalReturn); // (Debug)
							// Fertig strukturierten String ausgeben...
							that.bot.say(that.config.channels[0], finalReturn);
							// ...und Container leeren.
							finalReturn = "";
						}
						// Dritte Nachricht ausgeben.
						that.bot.say(that.config.channels[0], messages[task][2]);
					}
				}
				console.log("-------- NOTIFICATIONCHECK END \n"); // (Debug)
			});

			res.on('error', function (errorContent) {
				console.log("Error: " + errorContent); // (Debug)
			})
		});
		req.end();
	}

	// IRC-Client erstellen
	this.bot		= new irc.Client(that.config.server, that.config.nick, that.config.options);

	/**
		Twitch.TV-IRC Workaround

		Da der ursprüngliche Chat-Service von Twitch.TV(/Justin.TV) nicht auf einem IRC basierte
		und weiterhin mit dieser Software betrieben wird, nutzt dieser Bot vielmehr ein offenes
		"IRC-Interface" dieser Software, welches sich an einigen Stellen nicht wie ein nativer
		IRC-Server verhält. Beispielsweise werden beim Verbinden keine USERFLAGS gesendet, was
		beim IRC-Modul zu unendlichem Warten führt und nach 30 Sekunden ohne Autorisierung in 
		einem Kick vom Netzwerk resultiert. Daher kann nicht auf die callback-Funktion von irc.join()
		( https://node-irc.readthedocs.org/en/latest/API.html#'join' ) vertraut werden und dieser Bot
		betritt 5 Sekunden nach Erstellung der IRC-Instanz in "this.bot" manuell den angegebenen Raum.
	*/

	setTimeout(function() {
		that.bot.join(that.config.channels[0]);
	}, 5000);

	// Wirft eine Fehlermeldung im Konsolenfenster aus, sollte beim Beitritt ein Fehler auftreten
	this.bot.addListener('error', function(message) {
		    console.log('error: ', message);
	});

	// Fragt - 10 Sekunden nach Bot-Erstellung - alle 60 Sekunden nach den neusten Fanarts
	setTimeout(function() {
		setInterval(function() {
			that.notificationCheck("fanart");
		}, 1 * 60 * 1000);
	}, 10000);

	// Fragt - 40 Sekunden nach Bot-Erstellung - alle 60 Sekunden nach neuen Spenden
	setTimeout(function() {
		setInterval(function() {
			that.notificationCheck("donations");
		}, 1 * 60 * 1000);
	}, 40000);

	// Auf Chat-Zeile mit "@vote " im Anfang warten und dann verarbeiten
	this.bot.addListener("message", function(from, to, text, message) {	
		console.log(text.indexOf("@vote "));
		if (text.indexOf("@vote ") === 0) {
			text = text.replace("@vote ", "");
			that.parseChatRow(text, from);
		}
	});
}

vote = new Vote();